package com.geocal.ece.geocalc;

import com.geocal.ece.geocalc.Foyer.FragmentFoyer;

import org.junit.Assert;
import org.junit.Test;

import Modele.Installation;
import Modele.Pompe;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void  calculInstalationTest()
    {
        Pompe veri = new Pompe("Entreprise","Verticale",5,5,6);
        FragmentFormulaire form = new FragmentFormulaire();
        Installation installVerti = form.calculInstallVerticale(veri);

        Assert.assertEquals( veri,installVerti.getPompe());
    }

}