package Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by dtshilenge on 17/12/2016.
 */

public class DatabaseFoyer extends SQLiteOpenHelper {

    public static final String FOYER_ID = "id";
    public static final String FOYER_SURFACE = "surface";
    public static final String FOYER_NOMBRE_OCCUPANT = "nombre_occupants";
    public static final String FOYER_TYPE_CHAUDIERE = "type_chaudier";
    public static final String FOYER_TEMPERATURE = "temperature";
    public static final String FOYER_ADRESSE = "adresse";
    public static final String FOYER_TMIN = "FOYER_TMIN";
    public static final String FOYER_TMAX = "FOYER_TMAX";
    public static final String FOYER_TMOY = "FOYER_TMOY";
    public static final String FOYER_TSOL = "FOYER_TSOL";
    public static final String FOYER_TEAU = "FOYER_TEAU";
    public static final String FOYER_TBASE = "FOYER_TBASE";
    public static final String FOYER_DJU = "FOYER_DJU";
    public static final String FOYER_SAVE = "sauvegarde";
    public static final String FOYER_TITRE_RECHERCHE = "titre_recherche";
    public static final String FOYER_SURFACE_JARDIN = "FOYER_SURFACE_JARDIN";
    public static final String FOYER_ISOLATION = "FOYER_ISOLATION";

    public static final String FOYER_TABLE_NAME = "Foyer";

    public static final String FOYER_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + FOYER_TABLE_NAME + " (" +
                    FOYER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    FOYER_SURFACE + " INTEGER, " +
                    FOYER_SURFACE_JARDIN + " INTEGER, " +
                    FOYER_TYPE_CHAUDIERE + " TEXT, " +
                    FOYER_ISOLATION + " TEXT, " +
                    FOYER_TEMPERATURE + " INTEGER, " +
                    FOYER_NOMBRE_OCCUPANT + " INTEGER, " +
                    FOYER_ADRESSE + " TEXT," +
                    FOYER_SAVE + " INTEGER," +
                    FOYER_TMIN + " TEXT," +
                    FOYER_TMAX + " TEXT," +
                    FOYER_TMOY + " TEXT," +
                    FOYER_TSOL + " TEXT," +
                    FOYER_TEAU + " TEXT," +
                    FOYER_TBASE + " TEXT," +
                    FOYER_DJU + " TEXT," +
                    FOYER_TITRE_RECHERCHE + " TEXT );";

    public static final String FOYER_TABLE_DROP = "DROP TABLE IF EXISTS " + FOYER_TABLE_NAME + ";";

    public DatabaseFoyer(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(FOYER_TABLE_CREATE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(FOYER_TABLE_DROP);
        onCreate(db);
    }
}
