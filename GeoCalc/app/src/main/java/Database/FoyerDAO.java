package Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import Modele.Adresse;
import Modele.DonneesSol;
import Modele.Foyer;
import Modele.Isolation;
import Modele.TypeChaudiere;

/**
 * Created by dtshilenge on 17/12/2016.
 */

public class FoyerDAO {

    // chargement de données

    private SQLiteDatabase databaseSQLite;
    private DatabaseFoyer dbFoyer;

    public static final String FOYER_TABLE_NAME = "Foyer";

    public static final String FOYER_ID = "id";
    public static final String FOYER_SURFACE = "surface";
    public static final String FOYER_NOMBRE_OCCUPANT = "nombre_occupants";
    public static final String FOYER_TYPE_CHAUDIERE = "type_chaudier";
    public static final String FOYER_TEMPERATURE = "temperature";
    public static final String FOYER_ADRESSE = "adresse";
    public static final String FOYER_SAVE = "sauvegarde";
    public static final String FOYER_TITRE_RECHERCHE = "titre_recherche";
    public static final String FOYER_TMIN = "FOYER_TMIN";
    public static final String FOYER_TMAX = "FOYER_TMAX";
    public static final String FOYER_TMOY = "FOYER_TMOY";
    public static final String FOYER_TSOL = "FOYER_TSOL";
    public static final String FOYER_TEAU = "FOYER_TEAU";
    public static final String FOYER_TBASE = "FOYER_TBASE";
    public static final String FOYER_DJU = "FOYER_DJU";
    public static final String FOYER_SURFACE_JARDIN = "FOYER_SURFACE_JARDIN";
    public static final String FOYER_ISOLATION = "FOYER_ISOLATION";

    private String[] FOYER_ALL_COLUMNS = {
            FOYER_ID,
            FOYER_SURFACE,
            FOYER_NOMBRE_OCCUPANT,
            FOYER_TYPE_CHAUDIERE,
            FOYER_ADRESSE,
            FOYER_SAVE,
            FOYER_TITRE_RECHERCHE,
            FOYER_TMIN,
            FOYER_TMAX,
            FOYER_TMOY,
            FOYER_TSOL,
            FOYER_TEAU,
            FOYER_TBASE,
            FOYER_DJU,
            FOYER_SURFACE_JARDIN,
            FOYER_ISOLATION
    };

    // Si je décide de la mettre à jour, il faudra changer cet attribut
    protected final static int VERSION = 1;
    // Le nom du fichier qui représente ma base
    protected final static String NOM = "database.db";


    public FoyerDAO(Context pContext) {
        dbFoyer = new DatabaseFoyer(pContext, NOM, null, VERSION);
    }

    public SQLiteDatabase open() {
        // Pas besoin de fermer la dernière base puisque getWritableDatabase s'en charge
        databaseSQLite = dbFoyer.getWritableDatabase();
        return databaseSQLite;
    }

    public void close() {
        databaseSQLite.close();
    }

    public SQLiteDatabase getDb() {
        return databaseSQLite;
    }

    public void createFoyer(Foyer pFoyer)
    {
        ContentValues values = new ContentValues();

        values.put(FOYER_SURFACE,pFoyer.getSurface());
        values.put(FOYER_SURFACE_JARDIN,pFoyer.getSurfaceJardin());
        values.put(FOYER_NOMBRE_OCCUPANT,pFoyer.getNbOccupants());
        values.put(FOYER_TYPE_CHAUDIERE,  pFoyer.getTypeChaudiere().getmType());
        values.put(FOYER_ISOLATION,  pFoyer.getIsolation().getLibelle());
        values.put(FOYER_ADRESSE,pFoyer.getAdresse().getFullAdresse());
        values.put(FOYER_TMIN,String.valueOf(pFoyer.getDonneesSol().getTMin()));
        values.put(FOYER_TMAX,String.valueOf(pFoyer.getDonneesSol().getTMax()));
        values.put(FOYER_TMOY,String.valueOf(pFoyer.getDonneesSol().getTMoy()));
        values.put(FOYER_TSOL,String.valueOf(pFoyer.getDonneesSol().getTSol()));
        values.put(FOYER_TEAU,String.valueOf(pFoyer.getDonneesSol().getTEau()));
        values.put(FOYER_TBASE,String.valueOf(pFoyer.getDonneesSol().getTBase()));
        values.put(FOYER_DJU,String.valueOf(pFoyer.getDonneesSol().getDJU()));

        if(pFoyer.getId() == 0)
        {
            values.put(FOYER_SAVE,"0");
            values.put(FOYER_TITRE_RECHERCHE,"titre" + pFoyer.getTemperature() );
            databaseSQLite.insert(FOYER_TABLE_NAME, null, values);
        }
        else if(pFoyer.getId() > 0)
        {
            int save = pFoyer.isSauvegarde()? 1 : 0;

            values.put(FOYER_SAVE,save+"");
            values.put(FOYER_TITRE_RECHERCHE, pFoyer.getTitre() );
            databaseSQLite.update(FOYER_TABLE_NAME, values, FOYER_ID  + " = ?", new String[] {String.valueOf(pFoyer.getId())});
        }

    }

    public void sauvegardeFoyer(Foyer foyer)
    {
        ContentValues newValues = new ContentValues();
        newValues.put(FOYER_SAVE, "1");
        newValues.put(FOYER_TITRE_RECHERCHE,foyer.getTitre());

        databaseSQLite.update(FOYER_TABLE_NAME, newValues, FOYER_ID  + " = ?", new String[] {String.valueOf(foyer.getId())});
    }

    public void supprimerParId(int idToDelete)
    {
        databaseSQLite.delete(FOYER_TABLE_NAME, FOYER_ID + " = ?", new String[] {String.valueOf(idToDelete)});
    }

    public List<Foyer> getListFoyer(boolean isSave)
    {
        List<Foyer> listFoyer = new ArrayList<Foyer>();

        Cursor cursor = databaseSQLite.query(FOYER_TABLE_NAME,
                FOYER_ALL_COLUMNS, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Foyer foyer = convertCursorToFoyer(cursor);
            if(foyer.isSauvegarde() == isSave )
                listFoyer.add(foyer);

            cursor.moveToNext();
        }
        cursor.close();

        Collections.reverse(listFoyer);

        return   listFoyer;
    }
    private Foyer convertCursorToFoyer(Cursor cursor) {
        Foyer foyer = new Foyer();
        DonneesSol donnees = new DonneesSol(cursor.getFloat(cursor.getColumnIndex(FOYER_TMAX)),
                                            cursor.getFloat(cursor.getColumnIndex(FOYER_TMIN)),
                                            cursor.getFloat(cursor.getColumnIndex(FOYER_TMOY)),
                                            cursor.getFloat(cursor.getColumnIndex(FOYER_TEAU)),
                                            cursor.getFloat(cursor.getColumnIndex(FOYER_TSOL)),
                                            cursor.getFloat(cursor.getColumnIndex(FOYER_TBASE)),
                                            cursor.getFloat(cursor.getColumnIndex(FOYER_DJU)));
        foyer.setDonneesSol(donnees);
        String[] donneesAdresse = cursor.getString(cursor.getColumnIndex(FOYER_ADRESSE)).split("_");
        Adresse adresse = new Adresse(Integer.parseInt(donneesAdresse[0]),donneesAdresse[1],Integer.parseInt(donneesAdresse[2]), donneesAdresse[2].substring(0,1), donneesAdresse[3]);
        foyer.setAdresse(adresse);
        foyer.setId(cursor.getInt(cursor.getColumnIndex(FOYER_ID)));
        foyer.setSurface(cursor.getInt(cursor.getColumnIndex(FOYER_SURFACE)));
        foyer.setSurfaceJardin(cursor.getInt(cursor.getColumnIndex(FOYER_SURFACE_JARDIN)));
        foyer.setNbOccupants(cursor.getInt(cursor.getColumnIndex(FOYER_NOMBRE_OCCUPANT)));
        foyer.setTitre(cursor.getString(cursor.getColumnIndex(FOYER_TITRE_RECHERCHE)));

        int sauvegarde = cursor.getInt(cursor.getColumnIndex(FOYER_SAVE));
        foyer.setSauvegarde(ConvertToBool(sauvegarde));
        String typechaudiere = cursor.getString(cursor.getColumnIndex(FOYER_TYPE_CHAUDIERE));
        foyer.setTypeChaudiere(getTypeChaudiere(typechaudiere));
        foyer.setIsolation(getIsolation(cursor.getString(cursor.getColumnIndex(FOYER_ISOLATION))));

        return foyer;
    }

    private boolean ConvertToBool(int pValue)
    {
        if(pValue == 0)
            return  false;

        return true;
    }

    private TypeChaudiere getTypeChaudiere(String pType){
        for(TypeChaudiere type: TypeChaudiere.values()){
            if(type.getmType().equalsIgnoreCase(pType)){
                return type;
            }
        }
        return null;
    }

    private Isolation getIsolation(String pIsolation){
        for(Isolation iso: Isolation.values()){
            if(iso.getLibelle().equalsIgnoreCase(pIsolation)){
                return iso;
            }
        }
        return null;
    }

}
