package com.geocal.ece.geocalc.Installation;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.geocal.ece.geocalc.Geothermie.FragmentGeothermieDef;
import com.geocal.ece.geocalc.Geothermie.FragmentGeothermieLien;
import com.geocal.ece.geocalc.Geothermie.FragmentGeothermieUtilite;
import com.geocal.ece.geocalc.R;

/**
 * Created by dtshilenge on 27/01/2017.
 */

public class FragmentInstallationPage extends Fragment {

    private Context mContext;

    public FragmentInstallationPage() {
        // Required empty public constructor
    }

    FragmentTabHost mTabHost;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tabhost, container, false);

        mTabHost = (FragmentTabHost) view.findViewById(android.R.id.tabhost);
        mTabHost.setup(mContext,getChildFragmentManager(),android.R.id.tabcontent);
        mTabHost.addTab(mTabHost.newTabSpec("Horizontal").setIndicator("Horizontal"), FragmentInstallationOngletHorizontale.class, null );
        mTabHost.addTab(mTabHost.newTabSpec("Vertical").setIndicator("Vertical"), FragmentInstallationOngletVertical.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("Acquifère").setIndicator("Acquifère"), FragmentInstallationOngletAcquifere.class, null);

        return mTabHost;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTabHost = null;
    }


}
