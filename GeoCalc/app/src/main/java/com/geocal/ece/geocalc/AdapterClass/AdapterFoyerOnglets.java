package com.geocal.ece.geocalc.AdapterClass;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.geocal.ece.geocalc.R;

import java.util.List;

import Database.FoyerDAO;
import Modele.Foyer;

/**
 * Created by dtshilenge on 26/01/2017.
 */

public class AdapterFoyerOnglets extends BaseAdapter {

    private Context mContext;
    private List<Foyer> listfoyer;
    private FoyerDAO mfoyerData;
    private FragmentManager mFragmentManager;

    public AdapterFoyerOnglets()
    {

    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_foyer_recherche, parent, false);
        }
        if ( position % 2 == 0) {
            convertView.setBackgroundColor(Color.WHITE);
        } else {
            convertView.setBackgroundColor(Color.DKGRAY);
        }


        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return listfoyer.get(position);
    }
    @Override
    public long getItemId(int position) {
        return listfoyer.get(position).getId();
    }
    @Override
    public int getCount() {
        return listfoyer.size();
    }

}
