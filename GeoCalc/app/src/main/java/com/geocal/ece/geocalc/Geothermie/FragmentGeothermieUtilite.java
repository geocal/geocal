package com.geocal.ece.geocalc.Geothermie;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.geocal.ece.geocalc.R;

/**
 * Created by dtshilenge on 27/01/2017.
 */

public class FragmentGeothermieUtilite extends Fragment {

    Context mContext = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_geothermie_onglets, container, false);

        String espace =" <br> ";
        String myHtmlString = espace;

        myHtmlString = " <p> " +
                "Le but de notre application sera de pouvoir permettre à des utilisateurs"+
                "Ayant des maisons avec grands jardin de pouvoir couvrir leur consomation en terme d'électricité"+
                "Vous pourrez effectuer une simulation dans l'application pour pouvoir visualiser l'installation adequate et à "+
                "à combien vous pourrez reduire la polution "
                +"</p> ";

        myHtmlString += espace;
        myHtmlString += " <p> Comment ça marche ? </p> ";

        myHtmlString += espace;
        myHtmlString += " <p> C'est simple , Allez à l'accueil , saisissez votre adresse et le formulaire correspondant </p> ";

        TextView text = (TextView) v.findViewById(R.id.geothermie_avantages);
        text.setText(R.string.geothermie_utilite);



        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


}
