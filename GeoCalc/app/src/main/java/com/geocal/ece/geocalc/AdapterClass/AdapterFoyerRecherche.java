package com.geocal.ece.geocalc.AdapterClass;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.geocal.ece.geocalc.DialogClass.DialogFragmentRecheche;
import com.geocal.ece.geocalc.DialogClass.DialogFragmentTitre;
import com.geocal.ece.geocalc.DialogClass.FontManager;
import com.geocal.ece.geocalc.Foyer.FragmentFoyerRecherche;
import com.geocal.ece.geocalc.R;

import java.util.List;

import Database.FoyerDAO;
import Modele.Foyer;

/**
 * Created by dtshilenge on 19/01/2017.
 */

public class AdapterFoyerRecherche extends BaseAdapter implements DialogInterface.OnDismissListener {

    private Context mContext;
    public List<Foyer> listfoyer;
    private FoyerDAO mfoyerData;
    private FragmentManager mFragmentManager;

    public AdapterFoyerRecherche(Context context, FoyerDAO data, FragmentManager fragmentManager) {
        this.mContext = context;
        this.listfoyer = data.getListFoyer(false);
        this.mfoyerData = data;
        this.mFragmentManager = fragmentManager;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_foyer_recherche, parent, false);
        }
        if ( position % 2 == 0) {
            convertView.setBackgroundColor(Color.WHITE);
        } else {
            convertView.setBackgroundColor(Color.DKGRAY);
        }


        Typeface iconFont = FontManager.getTypeface(mContext, FontManager.FONTAWESOME);
        //FontManager.markAsIconContainer(convertView.findViewById(R.id.foyer_search_layout), iconFont);


        TextView text = (TextView) convertView.findViewById(R.id.item_search_text); //recognize your view like this
        text.setText(listfoyer.get(position).toString());

        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Foyer foyerGet = listfoyer.get(position);
               /* DialogFragmentRecheche dialogFragmentRecheche = new DialogFragmentRecheche();
                dialogFragmentRecheche.setFoyer(foyerGet);
                dialogFragmentRecheche.show(mFragmentManager,"Titre de recherche");*/

                FragmentFoyerRecherche.mListener.envoyerFoyerToFormulaire(foyerGet);
            }
        });


        //Handle buttons and add onClickListeners
        Button deleteBtn = (Button)convertView.findViewById(R.id.delete_btn);
        deleteBtn.setTypeface(iconFont);

        Button SaveBtn = (Button)convertView.findViewById(R.id.save_btn);
        SaveBtn.setTypeface(iconFont);

        deleteBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                mfoyerData.supprimerParId(listfoyer.get(position).getId());
                listfoyer.remove(position); //or some other task
                notifyDataSetChanged();
            }
        });

        SaveBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                DialogFragmentTitre dialog = new DialogFragmentTitre();
                dialog.setmFoyerToSave(listfoyer.get(position));
                dialog.show(mFragmentManager,"Titre de recherche");
                DialogFragmentTitre.isChanged = false;
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        //Fragment dialog had been dismissed
        notifyDataSetChanged();
    }

    @Override
    public Object getItem(int position) {
        return listfoyer.get(position);
    }
    @Override
    public long getItemId(int position) {
        return listfoyer.get(position).getId();
    }
    @Override
    public int getCount() {
        return listfoyer.size();
    }

}
