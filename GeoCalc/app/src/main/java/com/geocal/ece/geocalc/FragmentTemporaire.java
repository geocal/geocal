package com.geocal.ece.geocalc;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Modele.Adresse;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentTemporaire.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentTemporaire}factory method to
 * create an instance of this fragment.
 */
public class FragmentTemporaire extends Fragment {

    private Button vButton;
    private EditText vDepartement;
    private OnFragmentInteractionListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fragment_temporaire, container, false);
        vButton = (Button) rootView.findViewById(R.id.valider);
        vDepartement = (EditText) rootView.findViewById(R.id.departement);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        vButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(!vDepartement.getText().toString().matches("")){
                    Adresse adresse = new Adresse();
                    adresse.setmDepartement(vDepartement.getText().toString());
                   mListener.envoyerDepartement(adresse);
                }

            }});
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void envoyerDepartement(Adresse adresse);
    }
}
