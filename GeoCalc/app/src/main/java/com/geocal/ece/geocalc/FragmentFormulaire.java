package com.geocal.ece.geocalc;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import Database.FoyerDAO;
import Modele.Adresse;
import Modele.Foyer;
import Modele.Installation;
import Modele.Isolation;
import Modele.Pompe;
import Modele.Recapitulatif;
import Modele.TypeChaudiere;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link FragmentFormulaire} factory method to
 * create an instance of this fragment.
 */
public class FragmentFormulaire extends Fragment {

    private static final String OBJECT_KEY = "object_key";

    //Lien avec MainActivity
    private OnValidationFormulaire mListener;
    //Elements view
    private EditText vSurface;
    private EditText vSurfaceJardin;
    private EditText vNbHab;
    private TextView vMessageErreur;
    private Spinner vTypeInstall;
    private Spinner vIsolation;
    private Button vBoutonValider;
    private TextView vAdresse;
    Double surfaceNecessaire;

    private ArrayAdapter adapterType;

    private Map<String, Integer>  vCouts;
    private Map<String, Integer> vTRI;

    private Map<String, Double>  vEconomies;
    private Map<String, Double> vCO2;


    //Objet commun dans plusieurs méthodes
        //Infos foyer
    private Foyer mFoyer;
        //Conso de l'utilisateur
    private Double besoinECS;
    private Double besoinChauffage;
        //Puissance à envoyer au serveur
    private Double puissNomiMaj;
        //Pompe Horizontale réajustée ?
    private boolean pompeHchange;
        //Si oui, quel pourcentage couvert
    private Double pourcentCouv;
    //BDD
    FoyerDAO foyerData = null;

    ArrayAdapter adapterIsolation;

    private boolean isDataAlreadyInit = false;

    public interface OnValidationFormulaire {
        void envoyerPuissance(Double pPuissance, boolean pJusteHorizontal);
    }

    public static FragmentFormulaire newInstance(Foyer pFoyer) {
        FragmentFormulaire fragment = new FragmentFormulaire();
        Bundle args = new Bundle();
        args.putSerializable(OBJECT_KEY, pFoyer);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        foyerData = new FoyerDAO(context);
        foyerData.open();
        if (context instanceof OnValidationFormulaire) {
            mListener = (OnValidationFormulaire) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnValidationFormulaire");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mFoyer = (Foyer) getArguments().getSerializable(OBJECT_KEY);
        }
        vTRI = new HashMap<>();
        vTRI.put("horiz" , 8);
        vTRI.put("verti" , 13);
        vTRI.put("aqui" , 9);

        vCouts = new HashMap<>();
        vCouts.put("horiz" , 1030);
        vCouts.put("verti" , 1378);
        vCouts.put("aqui" , 1120);

        vEconomies = new HashMap<>();
        vEconomies.put("Gaz propane" , 0.196956394);
        vEconomies.put("Electrique" , 0.12869476);
        vEconomies.put("Fioul" , 0.12071993);
        vEconomies.put("Bois" , 0.068773778);
        vEconomies.put("Gaz naturel" , 0.074626866);
        vEconomies.put("Geothermie" , 0.029192274);
        vEconomies.put("Aquathermie" , 0.024290313);

        vCO2 = new HashMap<>();
        vCO2.put("Gaz propane" , 0.274);
        vCO2.put("Electrique" , 0.18);
        vCO2.put("Fioul" , 0.3);
        vCO2.put("Bois" , 0.355);
        vCO2.put("Gaz naturel" , 0.234);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_formulaire, container, false);
        vSurface= (EditText) view.findViewById(R.id.surface);
        vSurfaceJardin= (EditText) view.findViewById(R.id.surface_jardin);
        vNbHab= (EditText) view.findViewById(R.id.nb_hab);
        vMessageErreur =  (TextView) view.findViewById(R.id.errorMessage);
        vIsolation = (Spinner) view.findViewById(R.id.isolation);

        vTypeInstall =(Spinner) view.findViewById(R.id.type_chauff);
        adapterType = ArrayAdapter.createFromResource(getActivity(), R.array.type_chauffage, R.layout.spinner_style);
        adapterType.setDropDownViewResource(R.layout.spinner_dropdown_item);
        vTypeInstall.setAdapter(adapterType);

        adapterIsolation = ArrayAdapter.createFromResource(getActivity(), R.array.isolation, R.layout.spinner_style);
        adapterIsolation.setDropDownViewResource(R.layout.spinner_dropdown_item);
        vIsolation.setAdapter(adapterIsolation);
        vBoutonValider = (Button) view.findViewById(R.id.validerForm);
        vAdresse = (TextView) view.findViewById(R.id.adresse);

        if(mFoyer.getSurface() != 0)
        {
            initFormData(mFoyer);
        }

        return view;
    }


    public void initFormData(Foyer foyer)
    {
        if(foyer != null)
        {
            isDataAlreadyInit = true;
            vSurface.setText(foyer.getSurface() + "");
            vSurfaceJardin.setText(foyer.getSurfaceJardin()+ "");
            vNbHab.setText(foyer.getNbOccupants()+ "");


            ArrayAdapter myAdap = (ArrayAdapter) vIsolation.getAdapter();
            int spinnerPosition = myAdap.getPosition(foyer.getIsolation().getLibelle());
            vIsolation.setSelection(spinnerPosition);

            myAdap = (ArrayAdapter) vTypeInstall.getAdapter();
            spinnerPosition = myAdap.getPosition(foyer.getTypeChaudiere().getmType());
            vTypeInstall.setSelection(spinnerPosition);

            vAdresse.setText(foyer.getAdresse().toString());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        vBoutonValider.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if(formVide(vSurface, vNbHab, vSurfaceJardin)){
                    vMessageErreur.setText(R.string.erreur);
                }else
                {
                    int id = new Random().nextInt(3);
                    int surface = Integer.parseInt(vSurface.getText().toString());
                    int surfaceJardin = Integer.parseInt(vSurfaceJardin.getText().toString());
                    //Isolation isolation = Isolation.valueOf(vIsolation.getSelectedItem().toString());
                    TypeChaudiere typeChaudiere = getTypeChaudiere(vTypeInstall.getSelectedItem().toString());
                    Isolation isolation = getIsolation(vIsolation.getSelectedItem().toString());

                    Adresse adresse = mFoyer.getAdresse();
                    int nbOccupant = Integer.parseInt(vNbHab.getText().toString());

                    vMessageErreur.setText("");

                        mFoyer.setSurface(surface);
                        mFoyer.setIsolation(isolation);
                        mFoyer.setNbOccupants(nbOccupant);
                        mFoyer.setAdresse(adresse);
                        mFoyer.setTypeChaudiere(typeChaudiere);
                        mFoyer.setSurfaceJardin(surfaceJardin);
                    pompeHchange = false;
                    pourcentCouv = 100.0;

                    int conso = calculConso();
                    calculPompe();
                    mListener.envoyerPuissance(puissNomiMaj, false);

                    foyerData.createFoyer(mFoyer);
                }
            }
        });
    }



    private Isolation getIsolation(String pIsolation){
        for(Isolation iso: Isolation.values()){
            if(iso.getLibelle().equalsIgnoreCase(pIsolation)){
                return iso;
            }
        }
        return null;
    }

    private TypeChaudiere getTypeChaudiere(String pType){
        for(TypeChaudiere type: TypeChaudiere.values()){
            if(type.getmType().equalsIgnoreCase(pType)){
                return type;
            }
        }
        return null;
    }



    private boolean formVide(EditText pSurface, EditText pNbHab, EditText pTemp) {
        return (pSurface.getText().toString().matches(""))
                || (pNbHab.getText().toString().matches(""))
                || (pTemp.getText().toString().matches(""));

    }

    private int calculConso() {
        besoinECS = Float.parseFloat(getResources().getString(R.string.Cp)) * mFoyer.getNbOccupants()
                * 5.07 * (60 - mFoyer.getDonneesSol().getTEau());

        besoinChauffage = mFoyer.getIsolation().getValeur() * mFoyer.getSurface()
                * 0.06 * mFoyer.getDonneesSol().getDJU();

        return besoinECS.intValue() + besoinChauffage.intValue();
    }

    private void calculPompe() {
        puissNomiMaj = (1.2 * mFoyer.getIsolation().getValeur() * mFoyer.getSurface() * 2.5 *(17 - mFoyer.getDonneesSol().getTBase()))/ 1000;
        //On fait un appel du serveur avec
    }

    public Double calculInstallHorizontale(Double pPuissFrig, Double pPuissCal){
        Double puissance = 0.0;
        surfaceNecessaire = (pPuissFrig* (1000/30));
        int surfaceJardin = mFoyer.getSurfaceJardin();
        int tampon = surfaceNecessaire.intValue();
        if(surfaceJardin < tampon){
            puissance = pPuissCal * 0.9 * (surfaceJardin / surfaceNecessaire); //Calcul de la nouvelle puissance
            pompeHchange = true;
            pourcentCouv = 100 * (puissance / pPuissCal);
        }

        return puissance;
    }

    public Recapitulatif calculInstallation(boolean horizTrouve, boolean vertiTrouve, Pompe pHoriz, Pompe pVerti, boolean aquaTrouve, Installation install, Pompe pompeAqua){
        Recapitulatif recap = new Recapitulatif();
        recap.setPresenceH(horizTrouve);
        recap.setPresenceV(vertiTrouve);
        recap.setPresenceA(aquaTrouve);
        recap.setConso(besoinChauffage + besoinECS);
        recap.setFoyer(mFoyer);
        if(horizTrouve){
            Installation installHoriz = new Installation(pHoriz, "Horizontale", 4, 0.8 , pompeHchange, pourcentCouv);
            installHoriz.setCout((puissNomiMaj)*vCouts.get("horiz"));
            installHoriz.setTRI(Double.valueOf(vTRI.get("horiz")));
            installHoriz.setEconomieAnuelle((recap.getConso()*vEconomies.get(mFoyer.getTypeChaudiere().getmType())) -
                    (recap.getConso()*vEconomies.get(mFoyer.getTypeChaudiere().getmType())*(100-pourcentCouv.intValue())/100) -
                    (recap.getConso()*vEconomies.get("Geothermie")*(pourcentCouv.intValue())/100));
            installHoriz.setCo2((recap.getConso()*vCO2.get(mFoyer.getTypeChaudiere().getmType())) -
                    (recap.getConso()*vCO2.get(mFoyer.getTypeChaudiere().getmType())*(100-pourcentCouv.intValue())/100));
            installHoriz.setSurfaceNecessaire(surfaceNecessaire);
            recap.setInstallHoriz(installHoriz);
        }
        if(vertiTrouve){
            Installation installVerti = calculInstallVerticale(pVerti);
            installVerti.setCout((puissNomiMaj)*vCouts.get("verti"));
            installVerti.setTRI(Double.valueOf(vTRI.get("verti")));
            installVerti.setEconomieAnuelle((recap.getConso()*vEconomies.get(mFoyer.getTypeChaudiere().getmType())) -
                    (recap.getConso()*vEconomies.get("Geothermie")));
            installVerti.setCo2(recap.getConso() * vCO2.get(mFoyer.getTypeChaudiere().getmType()));
            recap.setInstallVerti(installVerti);
        }
        if(aquaTrouve){
            pompeAqua.setDebit(pompeAqua.getPuissanceFrigo() / 6.96);
            install.setCout((puissNomiMaj)*vCouts.get("aqui"));
            install.setTRI(Double.valueOf(vTRI.get("aqui")));
            install.setEconomieAnuelle((recap.getConso()*vEconomies.get(mFoyer.getTypeChaudiere().getmType())) -
                    (recap.getConso()*vEconomies.get("Aquathermie")));
            install.setCo2(recap.getConso() * vCO2.get(mFoyer.getTypeChaudiere().getmType()));
            install.setPompe(pompeAqua);
            recap.setInstallAqui(install);
        }
        return recap;
    }

    public Installation calculInstallVerticale(Pompe pVerti) {
        int nbSondes = 1;
        Double profondeur = (pVerti.getPuissanceFrigo()* 1000) / 55;
        Double profondeurMajorée;
        do{
            profondeurMajorée = ((profondeur/nbSondes) * 1.1) - 10.0;
            if(profondeurMajorée > 100){
                nbSondes++;
            }
        }while(profondeurMajorée > 100);

        return new Installation(pVerti, "Verticale", nbSondes, profondeurMajorée , false, 100.0);
    }

}
