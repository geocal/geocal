package com.geocal.ece.geocalc.DialogClass;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.geocal.ece.geocalc.AdapterClass.AdapterFoyerRecherche;
import com.geocal.ece.geocalc.AdapterClass.AdapterFoyerSauvegarde;
import com.geocal.ece.geocalc.Foyer.FragmentFoyer;
import com.geocal.ece.geocalc.Foyer.FragmentFoyerRecherche;
import com.geocal.ece.geocalc.R;

import Database.FoyerDAO;
import Modele.Foyer;

/**
 * Created by dtshilenge on 26/01/2017.
 */

public class DialogFragmentTitre extends DialogFragment {

    private FoyerDAO mFoyerData;
    private Foyer mFoyerToSave;
    private String titre ;
    public static boolean isChanged;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_fragment_titre, container, false);
        getDialog().setTitle("Titre recherche");

        Button save =  (Button) rootView.findViewById(R.id.sauvegarde_titre_boutton);
        final EditText text = (EditText) rootView.findViewById(R.id.sauvegarde_titre_recherche);
        text.setText(getTitre());

        save.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v)
            {
                titre = text.getText().toString();
                mFoyerToSave.setTitre(titre);
                mFoyerData.sauvegardeFoyer(mFoyerToSave);
                isChanged = true;
                dismiss();
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mFoyerData = new FoyerDAO(context);
        mFoyerData.open();
        isChanged = false;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Foyer getmFoyerToSave() {
        return mFoyerToSave;
    }

    public void setmFoyerToSave(Foyer mFoyerToSave) {
        this.mFoyerToSave = mFoyerToSave;
    }


}
