package com.geocal.ece.geocalc.Geothermie;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.geocal.ece.geocalc.R;

/**
 * Created by dtshilenge on 27/01/2017.
 */

public class FragmentGeothermieLien extends Fragment {

    Context mContext = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_geothermie_onglets, container, false);

        String espace =" <br> ";
        String myHtmlString = espace;

        TextView text = (TextView) v.findViewById(R.id.geothermie_avantages);
        text.setText(R.string.geothermie_web);



        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


}
