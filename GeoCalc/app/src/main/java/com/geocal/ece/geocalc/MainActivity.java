package com.geocal.ece.geocalc;

import com.geocal.ece.geocalc.Caroussel.FragmentInstallationCaroussel;
import com.geocal.ece.geocalc.Caroussel.FragmentItemImageDetails;
import com.geocal.ece.geocalc.Caroussel.FragmentItemImage;
import com.geocal.ece.geocalc.Foyer.FragmentFoyer;
import com.geocal.ece.geocalc.Foyer.FragmentFoyerRecherche;
import com.geocal.ece.geocalc.Foyer.FragmentFoyerSauvegarde;
import com.geocal.ece.geocalc.Geothermie.FragmentAPropos;
import com.geocal.ece.geocalc.Geothermie.FragmentContact;
import com.geocal.ece.geocalc.Geothermie.FragmentGeothermie;
import com.geocal.ece.geocalc.Installation.FragmentInstallationPage;
import android.app.ProgressDialog;


import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import Modele.Adresse;
import Modele.DonneesSol;
import Modele.Foyer;
import Modele.Installation;
import Modele.Pompe;
import Modele.Recapitulatif;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FragmentFormulaire.OnValidationFormulaire , FragmentTemporaire.OnFragmentInteractionListener
        , FragmentFoyer.OnFragmentFoyerInteractionListener, FragmentMap.OnFragmentMapInteractionListener, FragmentItemImage.OnItemFragmentInteraction,
        FragmentItemImageDetails.OnImageDetailFragmentInteraction, FragmentFoyerRecherche.OnFragmentFoyerInteractionListener,
        FragmentFoyerSauvegarde.OnFragmentFoyerSauvegardeInteractionListener {

    //Constantes liées au serveur
    public final String URL_SOL = "https://geocal-balthi75.c9users.io/sol.php";
    public final String URL_PAC = "https://geocal-balthi75.c9users.io/pac.php";
    public final String URL_AQUA = "https://geocal-balthi75.c9users.io/aqua.php";
    public static final int CONNECTION_TIMEOUT=100000;
    public static final int READ_TIMEOUT=150000;

    //Foyer que l'on complète au fur et à mesure
    public FragmentFormulaire fragFormulaire;
    public Foyer foyerActuel;
    public Pompe pompeHoriz;
    public Pompe pompeVerti;
    public Pompe pompeAqua;
    public Installation install;
    public static Recapitulatif recap;
    private LocationManager locationManager;


    //Booleans obligatoires car ils doivent être communs aux différentes connexions instanciées
    boolean pompeHtrouve;
    boolean pompeVtrouve;
    boolean pompeHvalide;
    boolean pompeVvalide;
    boolean pompeAtrouve;
    boolean pompeAvalide;
    boolean pompeDejaCherche;
    Double puissance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Partie chargement du fragment principal
        pompeDejaCherche = false;
        ajouterFragment(new FragmentMap());

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment currentFragment = new Fragment();
        if (id == R.id.nav_accueil) {
            currentFragment = new FragmentMap();
        } else if (id == R.id.nav_geo) {
            currentFragment = new FragmentGeothermie();
        } else if (id == R.id.nav_install) {
            currentFragment = new FragmentInstallationPage();
        } else if (id == R.id.nav_foyer) {
            currentFragment = new FragmentFoyer();
        } else if (id == R.id.nav_a_propos) {
            currentFragment = new FragmentAPropos();
        } else if (id == R.id.nav_contact) {
            currentFragment = new FragmentContact();
        }
        remplacerFragment(currentFragment);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void envoyerDepartement(Adresse adresse) {
        foyerActuel = new Foyer();
        foyerActuel.setAdresse(adresse);
        new AsyncLogin().execute(URL_SOL, "dep",adresse.getmDepartement() );
    }

    public void remplacerFragment(Fragment pFragSuivant) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container,pFragSuivant);
        transaction.addToBackStack(pFragSuivant.getClass().getName());
        transaction.commit();
    }

    public void ajouterFragment(Fragment pFragSuivant) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.fragment_container,pFragSuivant);
        transaction.addToBackStack(pFragSuivant.getClass().getName());
        transaction.commit();
    }

    @Override
    public void envoyerFoyerToFormulaire(Foyer foyer)
    {
        foyerActuel = foyer;
        fragFormulaire = FragmentFormulaire.newInstance(foyer);;
        remplacerFragment(fragFormulaire);
    }

    @Override
    public void envoyerFragment(Fragment fragment)
    {
        remplacerFragment(fragment);
    }

    @Override
    public void closeImageDetail()
    {
        onBackPressed();
    }

    @Override
    public void openOnglets()
    {
    }

    @Override
    public void envoyerPuissance(Double pPuissance, boolean pJusteHorizontal) {
        new AsyncLogin().execute(URL_PAC, "puissHoriz", String.valueOf(pPuissance));
        if(!pJusteHorizontal){
            puissance = pPuissance;
            new AsyncLogin().execute(URL_PAC, "puissVerti", String.valueOf(pPuissance));
            new AsyncLogin().execute(URL_AQUA, "depart", foyerActuel.getAdresse().getmDepartement());
        }
    }

    private class AsyncLogin extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(MainActivity.this);
        HttpURLConnection connect;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
            pdLoading.setMessage("\tChargement des données...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                // On insère l'url de la requete
                url = new URL(params[0]);

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return params[1]+ "_exception";
            }
            try {
                // On prépare la requête HTTP
                connect = (HttpURLConnection) url.openConnection();
                connect.setRequestMethod("POST");
                connect.setReadTimeout(READ_TIMEOUT);
                connect.setConnectTimeout(CONNECTION_TIMEOUT);
                // On indique que l'on veut envoyer des paramètres et recevoir  des données
                connect.setDoInput(true);
                connect.setDoOutput(true);

                // Open connection for sending data

                DataOutputStream writer = new DataOutputStream(
                        connect.getOutputStream());
                String vParametre = params[1] + "=" + params[2];
                writer.writeBytes(vParametre);
                writer.flush();
                writer.close();
                connect.connect();

            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                return params[1]+ "_exception";
            }

            try {

                int response_code = connect.getResponseCode();
                String test = connect.getResponseMessage();
                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = connect.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (params[1]+ "_"+result.toString());

                } else {

                    return (params[1]+ "_unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return params[1]+ "_exception";
            } finally {
                connect.disconnect();
            }


        }

        @Override
        protected void onPostExecute(String result) {

            pdLoading.dismiss();
            String[] parts = result.split("_");
            String part1 = parts[0];
            String part2 = parts[1];
            if (part2.equalsIgnoreCase("exception") || part2.equalsIgnoreCase("unsuccessful")) {
                if(part1.equalsIgnoreCase("dep")){

                    DonneesSol donnees= new DonneesSol(Float.parseFloat(getResources().getString(R.string.TMax)),
                                                        Float.parseFloat(getResources().getString(R.string.TMin)),
                                                        Float.parseFloat(getResources().getString(R.string.TMoy)),
                                                        Float.parseFloat(getResources().getString(R.string.TEau)),
                                                        Float.parseFloat(getResources().getString(R.string.TSol)),
                                                        Float.parseFloat(getResources().getString(R.string.Tbase)),
                                                        Float.parseFloat(getResources().getString(R.string.DJU)));
                    foyerActuel.setDonneesSol(donnees);
                }else if(part1.equalsIgnoreCase("puissHoriz")){
                    pompeHtrouve = false;
                    pompeHvalide = true;

                }else if(part1.equalsIgnoreCase("puissVerti")){
                    pompeVtrouve = false;
                    pompeVvalide = true;
                }else if (part1.equalsIgnoreCase("depart")){
                        pompeAtrouve = false;
                        pompeAvalide = true;
                }else if(part1.equalsIgnoreCase("puissAqua")){
                        pompeAtrouve = false;
                        pompeAvalide = true;
                }
            } else {
                /* Here launching another activity when login successful. If you persist login state
                use sharedPreferences of Android. and logout button to clear sharedPreferences.
                 */
                try {

                    // result is a variable which holds fetched json data.

                    if(part1.equalsIgnoreCase("dep")){
                        JSONArray jArray = new JSONArray(part2);
                        JSONObject json_data = jArray.getJSONObject(0);

                        DonneesSol donnees = new DonneesSol(Float.parseFloat(json_data.getString("TMax")),
                                                            Float.parseFloat(json_data.getString("TMin")),
                                                            Float.parseFloat(json_data.getString("TMoy")),
                                                            Float.parseFloat(json_data.getString("TEau")),
                                                            Float.parseFloat(json_data.getString("TSol")),
                                                            Float.parseFloat(json_data.getString("TBase")),
                                                            Float.parseFloat(json_data.getString("DJU")));

                        foyerActuel.setDonneesSol(donnees);
                    }else if(part1.equalsIgnoreCase("puissHoriz")){
                        if(part2.equalsIgnoreCase("[]")){
                            pompeHtrouve = false;
                            pompeHvalide = true;
                        }else {
                            JSONArray jArray = new JSONArray(part2);
                            JSONObject json_data = jArray.getJSONObject(0);
                            Double nouvPuissance = fragFormulaire.calculInstallHorizontale(Double.parseDouble(json_data.getString("PFrig")), Double.parseDouble(json_data.getString("PCal")));
                            if (nouvPuissance == 0) {
                                pompeHoriz = new Pompe(json_data.getString("Entreprise"),
                                        json_data.getString("Modele"),
                                        Float.parseFloat(json_data.getString("PCal")),
                                        Float.parseFloat(json_data.getString("PFrig")),
                                        Float.parseFloat(json_data.getString("COP")));
                                pompeHtrouve = true;
                                pompeHvalide = true;
                                pompeDejaCherche = false;
                            } else {
                                if(!pompeDejaCherche){
                                    envoyerPuissance(nouvPuissance, true);
                                    pompeDejaCherche = true;
                                    pompeHtrouve = true;
                                    pompeHvalide = false;
                                }else{
                                    pompeDejaCherche = false;
                                    pompeHtrouve = false;
                                    pompeHvalide = true;
                                }

                            }

                        }
                    }else if(part1.equalsIgnoreCase("puissVerti")){
                        if(part2.equalsIgnoreCase("[]")){
                            pompeVtrouve = false;
                            pompeVvalide = true;
                        }else {
                            JSONArray jArray = new JSONArray(part2);
                            JSONObject json_data = jArray.getJSONObject(0);
                            pompeVerti = new Pompe(json_data.getString("Entreprise"),
                                    json_data.getString("Modele"),
                                    Float.parseFloat(json_data.getString("PCal")),
                                    Float.parseFloat(json_data.getString("PFrig")),
                                    Float.parseFloat(json_data.getString("COP")));
                            pompeVtrouve = true;
                            pompeVvalide = true;
                        }
                    }else if (part1.equalsIgnoreCase("depart")){
                        if(part2.equalsIgnoreCase("[]")){
                            pompeAtrouve = false;
                            pompeAvalide = true;
                        }else {
                            new AsyncLogin().execute(URL_PAC, "puissAqua", String.valueOf(puissance));
                            JSONArray jArray = new JSONArray(part2);
                            JSONObject json_data = jArray.getJSONObject(0);
                            install = new Installation(json_data.getString("Nappe"),json_data.getString("Temperature"), json_data.getString("Profondeur"), "Aquifère");
                            pompeAtrouve = false;
                            pompeAvalide = false;
                        }
                    }else if(part1.equalsIgnoreCase("puissAqua")){
                        if(part2.equalsIgnoreCase("[]")){
                            pompeAtrouve = false;
                            pompeAvalide = true;
                        }else {
                            JSONArray jArray = new JSONArray(part2);
                            JSONObject json_data = jArray.getJSONObject(0);
                            pompeAqua = new Pompe(json_data.getString("Entreprise"),
                                    json_data.getString("Modele"),
                                    Float.parseFloat(json_data.getString("PCal")),
                                    Float.parseFloat(json_data.getString("PFrig")),
                                    Float.parseFloat(json_data.getString("COP")));
                            pompeAtrouve = true;
                            pompeAvalide = true;
                        }
                    }
                    } catch (JSONException e) {
                    }

            }
            if(part1.equalsIgnoreCase("dep")){
                fragFormulaire = FragmentFormulaire.newInstance(foyerActuel);
                remplacerFragment(fragFormulaire);
            }else{
                pageSuivante();
            }
        }

        private void pageSuivante() {
            if(pompeVvalide && pompeHvalide && pompeAvalide){
                recap = fragFormulaire.calculInstallation(pompeHtrouve, pompeVtrouve, pompeHoriz, pompeVerti, pompeAtrouve, install, pompeAqua);
                FragmentInstallationCaroussel frag = FragmentInstallationCaroussel.newInstance(recap);
                remplacerFragment(frag);
            }
        }
    }





}//fin de classe