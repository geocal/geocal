package com.geocal.ece.geocalc.Foyer;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.geocal.ece.geocalc.AdapterClass.AdapterFoyerRecherche;
import com.geocal.ece.geocalc.R;

import java.util.List;

import Database.FoyerDAO;
import Modele.Adresse;
import Modele.Foyer;

/**
 * Created by dtshilenge on 21/01/2017.
 */

public class FragmentFoyerRecherche extends Fragment {

    FoyerDAO mfoyerData = null;
    Context mContext = null;
    public static OnFragmentFoyerInteractionListener mListener = null;

    public interface OnFragmentFoyerInteractionListener
    {
        void envoyerFoyerToFormulaire(Foyer foyer);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_foyer_onglets, container, false);

        ListView listView = (ListView) v.findViewById(R.id.listView);
        AdapterFoyerRecherche adapter1  = new AdapterFoyerRecherche(mContext,mfoyerData,getFragmentManager());
        listView.setAdapter(adapter1);


        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mfoyerData = new FoyerDAO(context);
        mfoyerData.open();

        if (context instanceof OnFragmentFoyerInteractionListener) {
            mListener = (OnFragmentFoyerInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnValidationFormulaire");
        }
    }


}
