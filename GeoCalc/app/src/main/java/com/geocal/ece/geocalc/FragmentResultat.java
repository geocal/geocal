package com.geocal.ece.geocalc;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import Modele.Recapitulatif;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentResultat.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentResultat#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentResultat extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String RECAPITULATIF_KEY = "recapitulatif_key";

    // TODO: Rename and change types of parameters
    private Recapitulatif recapitulatif;
    private TextView cConso;
    private TextView cPompe;
    private TextView cInstall;
    private TextView cEco;
    private Button vEnthalpie;
    private Button vEntreprise;




    private OnFragmentInteractionListener mListener;

    public FragmentResultat() {
        // Required empty public constructor
    }

    public static FragmentResultat newInstance(Recapitulatif pRecapitulatif) {
        FragmentResultat fragment = new FragmentResultat();
        Bundle args = new Bundle();
        args.putSerializable(RECAPITULATIF_KEY, pRecapitulatif);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            recapitulatif = (Recapitulatif)  getArguments().getSerializable(RECAPITULATIF_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_resultat, container, false);
        cConso =  (TextView) view.findViewById(R.id.conso);
        cPompe =  (TextView) view.findViewById(R.id.champ_pompe);
        cInstall =  (TextView) view.findViewById(R.id.champ_install);
        cEco =  (TextView) view.findViewById(R.id.champ_eco);
        vEnthalpie = (Button) view.findViewById(R.id.enthalpie);
        vEntreprise = (Button) view.findViewById(R.id.entreprise);

        return view;
    }

    private String getBallonByNbrPerson(int nbrPerson)
    {
        String result = "";

        switch(nbrPerson)
        {
            case 1 : result = "75 L";
                break;
            case 2 : result = "100 L";
                break;
            case 3 : result = "150 L";
                break;
            case 4 : result = "250 L";
                break;
            case 5 : result = "300 L";
                break;
            case 6 : result = "300 L";
                break;
            case 7 : result = "500 L";
                break;
            default: result = "500 L";
                break;
        }

        return result;
    }

    @Override
    public void onStart() {
        super.onStart();

        String ballon = getBallonByNbrPerson(recapitulatif.getFoyer().getNbOccupants());

        cConso.setText("Votre conso annuelle: "+ recapitulatif.getConso().intValue() + " kW");

            switch(recapitulatif.getChoix()){
                case "Horizontale":
                    cPompe.setText(recapitulatif.getInstallHoriz().getPompe().getPompeInfo() + "Volume du ballon : " + ballon  );
                    cInstall.setText(recapitulatif.getInstallHoriz().getInstall());
                    cEco.setText(recapitulatif.getInstallHoriz().getEco());
                    break;
                case "Verticale":
                    cPompe.setText(recapitulatif.getInstallVerti().getPompe().getPompeInfo() + "Volume du ballon : " + ballon);
                    cInstall.setText(recapitulatif.getInstallVerti().getInstall());
                    cEco.setText(recapitulatif.getInstallVerti().getEco());
                    break;
                case "Aquifere":
                    cPompe.setText(recapitulatif.getInstallAqui().getPompe().getPompeInfo() + "Volume du ballon : " + ballon);
                    cInstall.setText(recapitulatif.getInstallAqui().getInstallAqua());
                    cEco.setText(recapitulatif.getInstallAqui().getEco());
                    break;
            }

        vEnthalpie.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://etao.fr/"));
                startActivity(browserIntent);
            }
        });

        vEntreprise.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.nextherm.fr/"));
                startActivity(browserIntent);
            }
        });

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
