package com.geocal.ece.geocalc.Caroussel;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.geocal.ece.geocalc.R;

import Modele.Recapitulatif;

/**
 * Created by dtshilenge on 27/01/2017.
 */

public class FragmentInstallationCaroussel extends Fragment {

    public final static int LOOPS = 1;
    public CarouselPagerAdapter adapter;
    public static ViewPager pager;
    public static int count = 3; //ViewPager items size
    private Context mContext;

    private static final String OBJECT_KEY = "object_key";
    public static Recapitulatif recap;
    private TextView vLabelConso;

    public static int[] imageArray;

    /**
     * You shouldn't define first page = 0.
     * Let define firstpage = 'number viewpager size' to make endless carousel
     */
    public static int FIRST_PAGE = 1;

    @Override
    public void onStart() {
        super.onStart();
        vLabelConso.setText(String.valueOf("Consommation Estimée : "  + recap.getConso().intValue() + " KWH "));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            recap = (Recapitulatif) getArguments().getSerializable(OBJECT_KEY);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static FragmentInstallationCaroussel newInstance(Recapitulatif pRecap) {
        FragmentInstallationCaroussel fragment = new FragmentInstallationCaroussel();
        Bundle args = new Bundle();
        args.putSerializable(OBJECT_KEY, pRecap);
        fragment.setArguments(args);
        imageArray = initItemImage(pRecap);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.carousel_main_caroussel, container, false);

        pager = (ViewPager) view.findViewById(R.id.myviewpager);
        vLabelConso = (TextView) view.findViewById(R.id.conso);

        //set page margin between pages for viewpager
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int pageMargin = ((metrics.widthPixels / 4) * 2);
        pager.setPageMargin(-pageMargin);

        CarouselPagerAdapter carouselPagerAdapter = new CarouselPagerAdapter(mContext, getChildFragmentManager());
        adapter = carouselPagerAdapter;
        pager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        pager.addOnPageChangeListener(adapter);

        // Set current item to the middle page so we can fling to both
        // directions left and right
        pager.setCurrentItem(FIRST_PAGE);
        pager.setOffscreenPageLimit(3);
        return view;
    }

    public static  int[] initItemImage(Recapitulatif pRecap) {
        recap = pRecap;
        count = 3; // 3 images de bases
        int[] arrayImage = new int [count];
        int i = 0;

        if(recap.getInstallAqui() != null)
        {
            arrayImage[i] = R.drawable.aquifere; i++;
        }

        if(recap.getInstallHoriz() != null)
        {
            arrayImage[i] = R.drawable.horizontale; i++;
        }

        if(recap.getInstallVerti() != null)
        {
            arrayImage[i] = R.drawable.verticale; i++;
        }

        if(arrayImage.length == 1) FIRST_PAGE = 0;
        else FIRST_PAGE = 1;

        count = i;

        return arrayImage;
    }


}
