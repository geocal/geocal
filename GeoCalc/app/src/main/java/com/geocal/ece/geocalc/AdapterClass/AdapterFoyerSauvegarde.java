package com.geocal.ece.geocalc.AdapterClass;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.geocal.ece.geocalc.DialogClass.DialogFragmentRecheche;
import com.geocal.ece.geocalc.DialogClass.DialogFragmentTitre;
import com.geocal.ece.geocalc.DialogClass.FontManager;
import com.geocal.ece.geocalc.Foyer.FragmentFoyerSauvegarde;
import com.geocal.ece.geocalc.R;

import java.util.List;

import Database.FoyerDAO;
import Modele.Foyer;

/**
 * Created by dtshilenge on 19/01/2017.
 */

public class AdapterFoyerSauvegarde extends BaseAdapter {
    Context context;
    public static List<Foyer> listfoyer;
    FoyerDAO mfoyerData;
    private FragmentManager mFragmentManager;

    public AdapterFoyerSauvegarde(Context context, FoyerDAO data, FragmentManager fragmentManager)
    {
        this.context = context;
        this.listfoyer = data.getListFoyer(true);
        this.mfoyerData = data;
        this.mFragmentManager = fragmentManager;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.fragment_foyer_sauvegarde, parent, false);
            //convertView = View.inflate(context, parent, false);
        }
        TextView text = (TextView) convertView.findViewById(R.id.item_saved_text); //recognize your view like this
        text.setText(listfoyer.get(position).getTitre());

        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Foyer foyerGet = listfoyer.get(position);
                FragmentFoyerSauvegarde.mListener.envoyerFoyerToFormulaire(foyerGet);
            }
        });

        Typeface iconFont = FontManager.getTypeface(context, FontManager.FONTAWESOME);
        //Handle buttons and add onClickListeners
        Button deleteBtn = (Button)convertView.findViewById(R.id.delete_btn);
        deleteBtn.setTypeface(iconFont);
        Button modifBtn = (Button)convertView.findViewById(R.id.modif_btn);
        modifBtn.setTypeface(iconFont);

        if ( position % 2 == 0) {
            convertView.setBackgroundColor(Color.WHITE);
        } else {
            convertView.setBackgroundColor(Color.DKGRAY);
        }

        deleteBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                mfoyerData.supprimerParId(listfoyer.get(position).getId());
                listfoyer.remove(position); //or some other task
                notifyDataSetChanged();
            }
        });
        modifBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                DialogFragmentTitre dialog = new DialogFragmentTitre();
                dialog.setmFoyerToSave(listfoyer.get(position));
                dialog.setTitre(listfoyer.get(position).getTitre());
                dialog.show(mFragmentManager,"Titre de recherche");
                listfoyer.get(position).setTitre(dialog.getTitre());
                notifyDataSetChanged();

            }
        });

        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return listfoyer.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public int getCount() {
        return listfoyer.size();
    }
}
