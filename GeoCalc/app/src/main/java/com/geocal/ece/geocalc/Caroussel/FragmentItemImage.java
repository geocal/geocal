package com.geocal.ece.geocalc.Caroussel;


        import android.annotation.SuppressLint;
        import android.content.Context;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.util.DisplayMetrics;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.TextView;

        import com.geocal.ece.geocalc.FragmentResultat;
        import com.geocal.ece.geocalc.R;

        import Modele.Installation;
        import Modele.Recapitulatif;

public class FragmentItemImage extends Fragment {

    private static final String POSITON = "position";
    private static final String SCALE = "scale";
    private static final String DRAWABLE_RESOURE = "resource";
    private static Context mContext;
    private OnItemFragmentInteraction mListener;
    private String mTypeInstal;


    private int screenWidth;
    private int screenHeight;

    private int[] imageArray = FragmentInstallationCaroussel.imageArray;

    public static Fragment newInstance(Context context, int pos, float scale) {
        Bundle b = new Bundle();
        b.putInt(POSITON, pos);
        b.putFloat(SCALE, scale);

        return Fragment.instantiate(context, FragmentItemImage.class.getName(), b);
    }

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWidthAndHeight();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        final int postion = this.getArguments().getInt(POSITON);
        float scale = this.getArguments().getFloat(SCALE);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(screenWidth / 2, screenHeight / 2);
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.carousel_fragment_image, container, false);

        TextView textView = (TextView) linearLayout.findViewById(R.id.text);
        CarouselLinearLayout root = (CarouselLinearLayout) linearLayout.findViewById(R.id.root_container);
        ImageView imageView = (ImageView) linearLayout.findViewById(R.id.pagerImg);

        textView.setText(getInstall(imageArray[postion]));
        imageView.setLayoutParams(layoutParams);
        imageView.setImageResource(imageArray[postion]);

        //handling click event
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*FragmentItemImageDetails item = new FragmentItemImageDetails();
                Bundle bundle = new Bundle();
                bundle.putInt(DRAWABLE_RESOURE, imageArray[postion]);
                item.setArguments(bundle);*/

                FragmentInstallationCaroussel.recap.setChoix(getChoxInstallation(imageArray[postion]));
                FragmentResultat fragmentResultat = FragmentResultat.newInstance(FragmentInstallationCaroussel.recap);
                mListener.envoyerFragment(fragmentResultat);
            }
        });

        root.setScaleBoth(scale);

        return linearLayout;
    }

    private String getInstall(int pImageCode) {
        String information = "";
        Recapitulatif recap = FragmentInstallationCaroussel.recap;

        if(pImageCode == R.drawable.verticale)
        {
            Installation Vertical = recap.getInstallVerti();
            if(Vertical != null)
            {
                information = recap.getInstallVerti().getInstall() + recap.getInstallVerti().getEcoCaroussel();
            }
        }
        else if(pImageCode == R.drawable.horizontale)
        {
            Installation Horizontale = recap.getInstallHoriz();
            if(Horizontale != null)
            {
                information = recap.getInstallHoriz().getInstall() + recap.getInstallHoriz().getEcoCaroussel();
            }
        }
        else if(pImageCode == R.drawable.aquifere)
        {
            Installation Acquifere = recap.getInstallAqui();
            if(Acquifere != null) {
                information = recap.getInstallAqui().getInformationAqua();
            }
        }
        return information;
    }

    private String getChoxInstallation(int codeImage) {
       String choix = null;

        switch(codeImage)
        {
            case R.drawable.horizontale :
            choix = "Horizontale";
            break;

            case R.drawable.verticale : choix = "Verticale";
            break;

            case R.drawable.aquifere : choix = "Aquifere";
            break;
        }

        return choix;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

        if (context instanceof OnItemFragmentInteraction) {
            mListener = (OnItemFragmentInteraction) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    private void getWidthAndHeight() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenHeight = displaymetrics.heightPixels;
        screenWidth = displaymetrics.widthPixels;
    }

    public interface OnItemFragmentInteraction {
        // TODO: Update argument type and name
        void envoyerFragment(Fragment fragment);
    }
}
