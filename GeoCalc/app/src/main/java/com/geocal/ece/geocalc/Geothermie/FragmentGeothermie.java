package com.geocal.ece.geocalc.Geothermie;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.geocal.ece.geocalc.R;

/**
 * Created by dtshilenge on 21/01/2017.
 */

public class FragmentGeothermie extends Fragment {

    private Context mContext;

    public FragmentGeothermie() {
        // Required empty public constructor
    }

    FragmentTabHost mTabHost;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tabhost, container, false);

        mTabHost = (FragmentTabHost) view.findViewById(android.R.id.tabhost);
        mTabHost.setup(mContext,getChildFragmentManager(),android.R.id.tabcontent);
        mTabHost.addTab(mTabHost.newTabSpec("Utilité").setIndicator("Utilité"), FragmentGeothermieUtilite.class, null );
        mTabHost.addTab(mTabHost.newTabSpec("Avantages").setIndicator("Avantages"), FragmentGeothermieDef.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("Web").setIndicator("Web"), FragmentGeothermieLien.class, null);

        return mTabHost;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
            mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTabHost = null;
    }

}
