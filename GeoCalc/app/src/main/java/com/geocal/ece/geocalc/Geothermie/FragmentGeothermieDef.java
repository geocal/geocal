package com.geocal.ece.geocalc.Geothermie;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.geocal.ece.geocalc.R;

/**
 * Created by dtshilenge on 27/01/2017.
 */

public class FragmentGeothermieDef extends Fragment {

    Context mContext = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_geothermie_onglets, container, false);

        String espace =" <br> ";
        String myHtmlString = "";

        myHtmlString +="<p>" +
                " Le terme \"géothermie\" vient des mots grecs \"gêo\" (terre) et \"thermos\" (chaud)." +
                " Il désigne le processus qui permet de capter en profondeur la chaleur terrestre pour la transformer en source d’électricité ou de chauffage. " +
                "L’exploitation de la chaleur souterraine se fait via un système de canalisations dans lesquelles circule un échange de courants chauds et froids." +
                " La géothermie est une source d’énergie renouvelable, car la chaleur qui provient du centre de la terre est illimitée."+
                " </p>";

        myHtmlString += espace;

        myHtmlString += "<p> La Géothermie, pour ou contre ? </p>";

        myHtmlString += espace;

        myHtmlString += " <p>\n" +
                "<strong>Avantages :</strong><br>\n" +
                "- La géothermie est une <a href=\"/environnement/les-mots-verts/energies-renouvelables-40381\" target=\"_self\">énergie renouvelable</a> et propre (pas de déchets à stocker, très peu d’émissions de <a href=\"/environnement/les-mots-verts/co2-gaz-effet-de-serre-38941\" target=\"_self\">CO2</a>)<br>\n" +
                "- La géothermie à très basse énergie est disponible dans tous les sous-sols de la planète<br>\n" +
                "<strong>Inconvénients :</strong><br>\n" +
                "- la difficulté d’accès à certains gisements d’énergie géothermique (contraintes géologiques, composition du sous-sol…)<br>- le déploiement à grande échelle de la technologie d’exploitation de l’énergie géothermique coûte encore très cher, car elle exige des forages à grande profondeur.</p>   ";

        TextView text = (TextView) v.findViewById(R.id.geothermie_avantages);
//        text.setText(Html.fromHtml(myHtmlString));
        text.setText(R.string.geothermie_avantage);


        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


}
