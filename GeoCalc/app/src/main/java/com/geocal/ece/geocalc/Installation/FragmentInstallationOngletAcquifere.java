package com.geocal.ece.geocalc.Installation;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.geocal.ece.geocalc.R;

/**
 * Created by dtshilenge on 27/01/2017.
 */

public class FragmentInstallationOngletAcquifere extends Fragment {

    Context mContext = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_installation_onglets, container, false);

        ImageView image = (ImageView) v.findViewById(R.id.image_installation_id);
        image.setImageResource(R.drawable.aquifere);

        TextView text = (TextView) v.findViewById(R.id.info_fragment_installation);
        text.setText(R.string.captage_acquifere);


        return v;
    }

    private String getHtmlText()
    {
        String result = "<p> acquifère </p>";
        return  result;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

}
