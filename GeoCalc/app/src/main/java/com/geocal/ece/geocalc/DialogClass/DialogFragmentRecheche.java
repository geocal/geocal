package com.geocal.ece.geocalc.DialogClass;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.geocal.ece.geocalc.FragmentFormulaire;
import com.geocal.ece.geocalc.R;

import Database.FoyerDAO;
import Modele.Foyer;

/**
 * Created by dtshilenge on 30/01/2017.
 */

public class DialogFragmentRecheche extends DialogFragment {

    private FoyerDAO mFoyerData;

    public Foyer getFoyer() {
        return foyer;
    }

    public void setFoyer(Foyer foyer) {
        this.foyer = foyer;
    }

    private Foyer foyer;

    private  FragmentFormulaire fragmentFormulaire;

    private EditText vSurface;
    private EditText vSurfaceJardin;
    private EditText vNbHab;
    private EditText vTemp;
    private TextView vMessageErreur;
    //private Spinner vTypeInstall;
    private Spinner vIsolation;
    private Button vBoutonValider;
    FoyerDAO foyerData = null;
    private FragmentFormulaire.OnValidationFormulaire mListener;
    private static String FOYER_OBJET_KEY="foyer_objet_key";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_formulaire, container, false);
        getDialog().setTitle("Recherche");

        View view = rootView;

        return rootView;
    }


}
