package com.geocal.ece.geocalc.Foyer;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.geocal.ece.geocalc.R;

import Database.FoyerDAO;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentFoyer.OnFragmentFoyerInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentFoyer} factory method to
 * create an instance of this fragment.
 */
public class FragmentFoyer extends Fragment {

    FoyerDAO foyerData = null;
    private Context mContext;
    private OnFragmentFoyerInteractionListener mListener;

    public FragmentFoyer() {
        // Required empty public constructor
    }

FragmentTabHost mTabHost;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tabhost, container, false);

        mTabHost = (FragmentTabHost) view.findViewById(android.R.id.tabhost);
        mTabHost.setup(mContext,getChildFragmentManager(),android.R.id.tabcontent);

        mTabHost.addTab(mTabHost.newTabSpec("Recherche").setIndicator("Recherche"), FragmentFoyerRecherche.class, null );
        mTabHost.addTab(mTabHost.newTabSpec("Sauvegarde").setIndicator("Sauvegarde"), FragmentFoyerSauvegarde.class, null);

        return mTabHost;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.openOnglets();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentFoyerInteractionListener) {
            mListener = (OnFragmentFoyerInteractionListener) context;
            foyerData = new FoyerDAO(context);
            foyerData.open();
            mContext = context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTabHost = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentFoyerInteractionListener {
        // TODO: Update argument type and name
        void openOnglets();
    }
}
