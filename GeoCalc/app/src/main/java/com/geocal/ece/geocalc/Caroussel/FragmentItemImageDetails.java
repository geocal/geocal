package com.geocal.ece.geocalc.Caroussel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.geocal.ece.geocalc.R;

/**
 * Created by dtshilenge on 27/01/2017.
 */

public class FragmentItemImageDetails extends Fragment {

    private ImageView imageView;
    private Button button;
    private static final String DRAWABLE_RESOURE = "resource";
    private OnImageDetailFragmentInteraction mListener;
    private Context mContext ;
    private ViewPager mPager;


    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.carousel_fragment_detail_image, container, false);

        imageView = (ImageView) view.findViewById(R.id.img);
        button = (Button) view.findViewById(R.id.btnClose);

        Bundle bundle = this.getArguments();

        int drawbleResource = bundle.getInt(DRAWABLE_RESOURE, 0);
        imageView.setImageResource(drawbleResource);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              // getActivity().finish();
                mListener.closeImageDetail();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

        if (context instanceof OnImageDetailFragmentInteraction) {
            mListener = (OnImageDetailFragmentInteraction) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnImageDetailFragmentInteraction");
        }

    }

    public interface OnImageDetailFragmentInteraction {
        // TODO: Update argument type and name
        void closeImageDetail();
    }
}
