package Modele;


import java.io.Serializable;

/**
 * Created by Anastasion on 22/01/2017.
 */

public class DonneesSol implements Serializable {

    private float TMax;

    private float TMin;

    private float TMoy;

    private float TEau;

    private float TSol;

    private float TBase;

    private float DJU;

    public DonneesSol(float TMax, float TMin, float TMoy, float TEau, float TSol, float TBase, float DJU) {
        this.TMax = TMax;
        this.TMin = TMin;
        this.TMoy = TMoy;
        this.TEau = TEau;
        this.TSol = TSol;
        this.TBase = TBase;
        this.DJU = DJU;
    }

    public float getTMax() {
        return TMax;
    }

    public void setTMax(float TMax) {
        this.TMax = TMax;
    }

    public float getTMin() {
        return TMin;
    }

    public void setTMin(float TMin) {
        this.TMin = TMin;
    }

    public float getTMoy() {
        return TMoy;
    }

    public void setTMoy(float TMoy) {
        this.TMoy = TMoy;
    }

    public float getTEau() {
        return TEau;
    }

    public void setTEau(float TEau) {
        this.TEau = TEau;
    }

    public float getTSol() {
        return TSol;
    }

    public void setTSol(float TSol) {
        this.TSol = TSol;
    }

    public float getDJU() {
        return DJU;
    }

    public void setDJU(float DJU) {
        this.DJU = DJU;
    }

    public float getTBase() {
        return TBase;
    }

    public void setTBase(float TBase) {
        this.TBase = TBase;
    }
}
