package Modele;

import java.io.Serializable;

/**
 * Created by Anastasion on 11/12/2016.
 */

public class Recapitulatif implements Serializable{

    private String choix;

    private Installation installVerti;

    private Installation installHoriz;

    private Installation installAqui;

    private boolean presenceH;

    private boolean presenceV;

    private boolean presenceA;

    private Double conso;

    private Foyer foyer;

    public Installation getInstallVerti() {
        return installVerti;
    }

    public void setInstallVerti(Installation installVerti) {
        this.installVerti = installVerti;
    }

    public Installation getInstallHoriz() {
        return installHoriz;
    }

    public void setInstallHoriz(Installation installHoriz) {
        this.installHoriz = installHoriz;
    }

    public boolean isPresenceH() {
        return presenceH;
    }

    public void setPresenceH(boolean presenceH) {
        this.presenceH = presenceH;
    }

    public boolean isPresenceV() {
        return presenceV;
    }

    public void setPresenceV(boolean presenceV) {
        this.presenceV = presenceV;
    }

    public Double getConso() {
        return conso;
    }

    public void setConso(Double conso) {
        this.conso = conso;
    }

    public Foyer getFoyer() {
        return foyer;
    }

    public void setFoyer(Foyer foyer) {
        this.foyer = foyer;
    }

    public Installation getInstallAqui() {
        return installAqui;
    }

    public void setInstallAqui(Installation installAqui) {
        this.installAqui = installAqui;
    }

    public boolean isPresenceA() {
        return presenceA;
    }

    public void setPresenceA(boolean presenceA) {
        this.presenceA = presenceA;
    }

    public String getChoix() {
        return choix;
    }

    public void setChoix(String choix) {
        this.choix = choix;
    }
}
