package Modele;

import java.io.Serializable;

/**
 * Created by Anastasion on 11/12/2016.
 */

public enum Isolation implements Serializable {

    Isole ("Isolation spéciale", 0.3),
    RT2012 ("Après 2012", 0.5),
    RT2005 ("Entre 2005 et 2012", 0.75),
    RT2000 ("Entre 2000 et 2005", 0.8),
    RT1990 ("Entre 1990 et 2000", 0.95),
    RT1983 ("Entre 1983 et 1990", 1.15),
    RT1974 ("Entre 1974 et 1983", 1.4),
    NonIsole ("Non isolé", 1.8);

    private String libelle ="";
    private double valeur = 1.4;

    Isolation(String pLibelle, double pValeur) {

        this.libelle = pLibelle;
        this.valeur = pValeur;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public double getValeur() {
        return valeur;
    }

    public void setValeur(double valeur) {
        this.valeur = valeur;
    }
}
