package Modele;

import java.io.Serializable;

/**
 * Created by Anastasion on 11/12/2016.
 */

public class Installation implements Serializable {

    private Pompe pompe;

    private String nappe;

    private String temperature;

    private String prof;

    private Double surfaceNecessaire;

    private Double cout;

    private Double profondeur;

    private int nbBranches;

    private String type;

    private Double pourcentCouv;

    private boolean couvPartiel;

    private Double economieAnuelle;

    private Double co2;

    private Double TRI;


    public Installation(Pompe pompe, String type, int nbBranches, Double profondeur, boolean couvPartiel, Double pourcentCouv) {
        this.pompe = pompe;
        this.type = type;
        this.nbBranches = nbBranches;
        this.profondeur = profondeur;
        this.couvPartiel = couvPartiel;
        this.pourcentCouv = pourcentCouv;
    }

    public Installation(String nappe, String temperature, String prof, String type) {
        this.type = type;
        this.nappe = nappe;
        this.temperature = temperature;
        this.prof = prof;
    }

    public String getInformationAqua(){
        String information = "";
        String profond = "";
        if(this.prof.contains("et")){
            profond = "Entre " + this.prof + " mètres";
        }else{
            profond = this.prof + " mètres";
        }

        information = "Type : " + this.type + "\n";
        information += "Cout : " + this.cout.intValue() + "€\n";
        information += "Profondeur : " + profond + "\n";
        information += "Economie Anuelle : " + this.economieAnuelle.intValue() + " €\n";
        information += "Rentabilité au bout de " + this.TRI.intValue() + " ans\n";

        return information;
    }

    public String getInstallAqua(){
        String information = "";
        String profond = "";
        String temper="";
        if(this.prof.contains("et")){
            profond = "Entre " + this.prof + " mètres";
        }else{
            profond = this.prof + " mètres";
        }

        if(this.temperature.contains("et")){
            temper = "Entre " + this.temperature + "°C";
        }else{
            temper = this.temperature + "°C";
        }

        information = "Type : " + this.type + "\n";
        information += "Investissement : " + this.cout.intValue() + "€\n";
        information += "Profondeur : " + profond + "\n";
        information += "Sous la nappe : " + this.nappe + "\n";
        information += "Température de nappe : " + temper + "\n";

        return information;
    }

    public String getInstall(){
        String information = "";

        information = "Type : " + this.type + "\n";
        information += "Investissement : " + this.cout.intValue() + "€\n";
        if(this.type.equalsIgnoreCase("verticale")) {
            information += "Profondeur : " + this.profondeur.intValue() + " mètres\n";
            information += "Nombre de sondes : " + this.nbBranches + "\n";
        }else{
            information += "Surface nécessaire : " + this.surfaceNecessaire.intValue() + "m²\n";
        }
        information += "Consommation couverte : " + this.pourcentCouv.intValue() + "%\n";

        return information;
    }

    public String getEco(){
        String information = "";

        information = "Financièrement : " + this.economieAnuelle.intValue() + " € par an\n";
        information += "Ecologiquement : " + this.co2.intValue() + "Kg de CO2 par an\n";
        information += "Rentabilité au bout de " + this.TRI.intValue() + " ans\n";

        return information;
    }

    public String getEcoCaroussel(){
        String information = "";

        information = "Economie annuelle : " + this.economieAnuelle.intValue() + " € par an\n";
        information += "Rentabilité au bout de " + this.TRI.intValue() + " ans\n";

        return information;
    }


    public Double getCout() {
        return cout;
    }

    public void setCout(Double cout) {
        this.cout = cout;
    }

    public Double getProfondeur() {
        return profondeur;
    }

    public void setProfondeur(Double profondeur) {
        this.profondeur = profondeur;
    }

    public int getNbBranches() {
        return nbBranches;
    }

    public void setNbBranches(int nbBranches) {
        this.nbBranches = nbBranches;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getPourcentCouv() {
        return pourcentCouv;
    }

    public void setPourcentCouv(Double pourcentCouv) {
        this.pourcentCouv = pourcentCouv;
    }

    public boolean isCouvPartiel() {
        return couvPartiel;
    }

    public void setCouvPartiel(boolean couvPartiel) {
        this.couvPartiel = couvPartiel;
    }

    public Pompe getPompe() {
        return pompe;
    }

    public void setPompe(Pompe pompe) {
        this.pompe = pompe;
    }


    public Double getTRI() {
        return TRI;
    }

    public void setTRI(Double TRI) {
        this.TRI = TRI;
    }

    public Double getEconomieAnuelle() {
        return economieAnuelle;
    }

    public void setEconomieAnuelle(Double economieAnuelle) {
        this.economieAnuelle = economieAnuelle;
    }

    public Double getCo2() {
        return co2;
    }

    public void setCo2(Double co2) {
        this.co2 = co2;
    }

    public String getNappe() {
        return nappe;
    }

    public void setNappe(String nappe) {
        this.nappe = nappe;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getProf() {
        return prof;
    }

    public void setProf(String prof) {
        this.prof = prof;
    }


    public Double getSurfaceNecessaire() {
        return surfaceNecessaire;
    }

    public void setSurfaceNecessaire(Double surfaceNecessaire) {
        this.surfaceNecessaire = surfaceNecessaire;
    }
}
