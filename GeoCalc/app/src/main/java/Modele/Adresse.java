package Modele;

import java.io.Serializable;

/**
 * Created by Anastasion on 11/12/2016.
 */

public class Adresse implements Serializable{

    private int mNum;

    private String mVoie;

    private int mCodePostal;

    private String mDepartement;
    private String mVille;


    private String mCountry;

    public Adresse()
    {
    }

    public Adresse(int mNum,String mVoie,int mCodePostal, String mDepartement, String mVille )
    {
        this.mNum = mNum;
        this.mVoie = mVoie;
        this.mCodePostal = mCodePostal;
        this.mDepartement = mDepartement;
        this.mVille =mVille;
    }

    @Override
    public String toString()
    {
        return  mNum + " "+ mVoie + " " + mCodePostal + " , " + mVille;
    }

    public String getPartAdress()
    {
        return  mNum + " "+ mVoie + " " + mCodePostal;
    }

    public String getFullAdresse()
    {
        return mNum + "_" + mVoie + "_" + mCodePostal + "_" + mVille;
    }


    public String getmCountry() {
        return mCountry;
    }

    public void setmCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    public int getmNum() {
        return mNum;
    }

    public void setmNum(int mNum) {
        this.mNum = mNum;
    }

    public String getmVoie() {
        return mVoie;
    }

    public void setmVoie(String mVoie) {
        this.mVoie = mVoie;
    }

    public void setmCodePostal(int mCodePostal) {
        this.mCodePostal = mCodePostal;
    }

    public int getmCodePostal() {
        return mCodePostal;
    }

    public String getmDepartement() {
        return mDepartement;
    }

    public void setmDepartement(String mDepartement) {
        this.mDepartement = mDepartement;
    }

    public String getmVille() {
        return mVille;
    }

    public void setmVille(String mVille) {
        this.mVille = mVille;
    }
}
