package Modele;

import java.io.Serializable;

/**
 * Created by Anastasion on 11/12/2016.
 */

public enum TypeChaudiere implements Serializable{

    Gaz ("Gaz propane"),
    GazNaturel ("Gaz naturel"),
    Bois ("Bois"),
    Fioul ("Fioul"),
    Electrique ("Electrique");

    private String mType ="";

    TypeChaudiere(String pType) {
        this.mType = pType;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }
}
