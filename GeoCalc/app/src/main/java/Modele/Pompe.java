package Modele;

import java.io.Serializable;

/**
 * Created by Anastasion on 11/12/2016.
 */

public class Pompe implements Serializable {

    private String modele;

    private Double debit;

    private String entreprise;

    private Double puissance;

    private double puissanceFrigo;

    private double cop;

    public Pompe(String entreprise, String modele, double puissance, double puissanceFrigo, double cop) {
        this.entreprise = entreprise;
        this.modele = modele;
        this.puissance = puissance;
        this.puissanceFrigo = puissanceFrigo;
        this.cop = cop;
    }

    public String getPompeInfo(){
        String information = "";

        information = "Marque : " + this.entreprise + "\n";
        information += "Modèle: " + this.modele + "\n";
        information += "Puissance : " + this.puissance.intValue() + " kW\n";
        String newDeb ="";
        if(this.debit !=null){
            if(String.valueOf(this.debit).contains(".")) {
                String[] deb = String.valueOf(this.debit).split("\\.");
                newDeb = deb[0] + "." + deb[1].substring(0, 2);
            }else{
                newDeb = String.valueOf(this.debit);
            }
            information += "Débit : " + newDeb + " m3/h\n";
        }
        return information;
    }


    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public Double getPuissance() {
        return puissance;
    }

    public void setPuissance(Double puissance) {
        this.puissance = puissance;
    }

    public double getPuissanceFrigo() {
        return puissanceFrigo;
    }

    public void setPuissanceFrigo(double puissanceFrigo) {
        this.puissanceFrigo = puissanceFrigo;
    }

    public double getCop() {
        return cop;
    }

    public void setCop(double cop) {
        this.cop = cop;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }
}
