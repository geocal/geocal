package Modele;

import java.io.Serializable;

/**
 * Created by Anastasion on 11/12/2016.
 */

public class Foyer implements Serializable {

    private int surface;

    private int id;

    private int nbOccupants;

    private int surfaceJardin;

    private Isolation isolation;

    private TypeChaudiere typeChaudiere;

    private int temperature;

    private Adresse adresse;

    private boolean sauvegarde;

    private DonneesSol donneesSol;


    private String titre;


    public Foyer(int Id,int Surface,int Temperature,int NbOccupants,TypeChaudiere TypeChaudiere, Adresse Adresse)
    {
        temperature = Temperature;
        surface =Surface;
        id = Id;
        nbOccupants = NbOccupants;
        typeChaudiere = TypeChaudiere;
        adresse = Adresse;
    }

    public Foyer()
    {
        id = 0;
    }

    @Override
    public String toString() {
        return  this.adresse.getPartAdress() + " | " + this.surface + "m2,  " + this.nbOccupants + "p,  " + this.temperature + "°";
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String mTitre) {
        this.titre = mTitre;
    }

    public void remplirDonneesMaison(Foyer pFoyer){
        this.surface =pFoyer.getSurface();
        this.surfaceJardin =pFoyer.getSurfaceJardin();
        this.temperature = pFoyer.getTemperature();
        this.isolation = pFoyer.getIsolation();
        this.nbOccupants = pFoyer.getNbOccupants();
        this.adresse = pFoyer.getAdresse();
    }

    public int getSurface() {
        return surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNbOccupants() {
        return nbOccupants;
    }

    public void setNbOccupants(int nbOccupants) {
        this.nbOccupants = nbOccupants;
    }

    public int getSurfaceJardin() {
        return surfaceJardin;
    }

    public void setSurfaceJardin(int surfaceJardin) {
        this.surfaceJardin = surfaceJardin;
    }

    public TypeChaudiere getTypeChaudiere() {
        return typeChaudiere;
    }

    public void setTypeChaudiere(TypeChaudiere typeChaudiere) {
        this.typeChaudiere = typeChaudiere;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public boolean isSauvegarde() {
        return sauvegarde;
    }

    public void setSauvegarde(boolean sauvegarde) {
        this.sauvegarde = sauvegarde;
    }

    public DonneesSol getDonneesSol() {
        return donneesSol;
    }

    public void setDonneesSol(DonneesSol donneesSol) {
        this.donneesSol = donneesSol;
    }

    public Isolation getIsolation() {
        return isolation;
    }

    public void setIsolation(Isolation isolation) {
        this.isolation = isolation;
    }
}
