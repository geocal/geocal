package com.geocal.ece.geocalc;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import com.google.android.gms.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Modele.Adresse;


/**
 * A simple {@link SupportMapFragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentMap.OnValidationAdresse} interface
 * to handle interaction events.
 * Use the {@link FragmentMap} factory method to
 * create an instance of this fragment.s
 */
public class FragmentMap extends android.support.v4.app.Fragment implements OnMapReadyCallback , LocationListener{

    private static Address mAdresseChoose;
    private static Adresse mAdresseChooseNotGoogle;
    private android.support.v4.app.FragmentActivity parentContext;
    //private OnFragmentMapInteractionListener mListener;

    private MapView mMapView;
    private GoogleMap googleMap;

    private Location CurrentLocation;
    private ArrayList<Adresse> mListAdressesAutoComplete = new ArrayList<Adresse>();
    private OnFragmentMapInteractionListener mListener;
    private AutoCompleteTextView locationSearch;
    private ArrayAdapter adapter;
    private List<Address> addressListGoogleMap;

    @Override
    public void onLocationChanged(Location location) {
        double latitute = location.getLatitude();
        double longitude = location.getLongitude();
    }


    public interface OnValidationAdresse {
        void validerAdresse(String departement);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMapView.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        LatLng paris = new LatLng(48.8, 2.4);

        if( CurrentLocation != null)
        {
            paris = new LatLng(CurrentLocation.getLongitude(),CurrentLocation.getLatitude());
        }
        this.googleMap.addMarker(new MarkerOptions().position(paris).title("Marker Title").snippet("Marker Description"));

            // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(paris).zoom(12).build();
        this.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_map, container, false);
        mMapView = (MapView) view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(this);

        if (ContextCompat.checkSelfPermission(parentContext, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
     //       googleMap.setMyLocationEnabled(true);
       //     CurrentLocation = googleMap.getMyLocation();
        }

        else {
        // Show rationale and request permission.
        }


        Button vBoutonValider = (Button) view.findViewById(R.id.valider);
        locationSearch = (AutoCompleteTextView) view.findViewById(R.id.fieldSearch);
        adapter = new ArrayAdapter(parentContext,android.R.layout.simple_list_item_1,mListAdressesAutoComplete);
        locationSearch.setAdapter(adapter);

        locationSearch.addTextChangedListener(new TextWatcher() {
           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {
               //here is your code
            onMapSearch();
           }
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count,
                                         int after) {
               // TODO Auto-generated method stub
           }
           @Override
           public void afterTextChanged(Editable s) {
               // TODO Auto-generated method stub

           }
       });

        locationSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(addressListGoogleMap != null)
                {
                    mAdresseChoose = addressListGoogleMap.get(i);
                    mAdresseChooseNotGoogle = mListAdressesAutoComplete.get(i);
                    updateGoogleMapPOstion(mAdresseChoose);
                    InputMethodManager imm = (InputMethodManager) parentContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        });


        vBoutonValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mAdresseChoose != null && mAdresseChooseNotGoogle != null)
                {
                    String postalcode = mAdresseChoose.getPostalCode();
                    mAdresseChooseNotGoogle.setmDepartement(postalcode.substring(0,2));

                    if (postalcode.length() == 5) {
                         mListener.envoyerDepartement(mAdresseChooseNotGoogle);
                    } else
                    {

                    }
                }
                else
                {
                    Toast.makeText(parentContext,"saisir une adresse valide", Toast.LENGTH_LONG);
                }
            }
        });

        final RadioButton normalMap = (RadioButton) view.findViewById(R.id.viewMap);
        normalMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNormalMap(view);
            }
        });

        RadioButton hybridMap = (RadioButton) view.findViewById(R.id.viewHybridMap);
        hybridMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onHybridMap(view);
            }
        });
        RadioButton terrainMap = (RadioButton) view.findViewById(R.id.viewTerrain);
        terrainMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onTerrainMap(view);
            }
        });
        RadioButton satelliteMap = (RadioButton) view.findViewById(R.id.viewSatellite);
        satelliteMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSatelliteMap(view);
            }
        });

        return view;
    }

    public void onMapSearch(){
        String location = "";
        if(locationSearch != null) location = locationSearch.getText().toString();
        addressListGoogleMap = null;
        if (location != null && !location.isEmpty()   ) {
            if(location.length() > 4 ){
            Geocoder geocoder = new Geocoder(parentContext);
            try {
                addressListGoogleMap = geocoder.getFromLocationName(location, 10);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(addressListGoogleMap != null)
                mListAdressesAutoComplete = getListAdresse(addressListGoogleMap);
        }
        adapter = new ArrayAdapter(parentContext,android.R.layout.simple_list_item_1,mListAdressesAutoComplete);
        locationSearch.setAdapter(adapter);
        locationSearch.showDropDown();
        }
    }

    private void updateGoogleMapPOstion(Address address ) {
        googleMap.clear();
        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
        googleMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

    }

    private ArrayList<Adresse> getListAdresse(List<Address> addressList) {
        ArrayList<Adresse> list = new ArrayList<Adresse>();

        if( addressList.size() > 0)
        {
            for(int i=0;i<addressList.size();i++)
            {
                Address currentAdresse = addressList.get(i);
                String postalCode =  currentAdresse.getPostalCode();

                if(postalCode != null)
                {
                    if(postalCode.length() == 5 )
                    {
                        int numero = 1;

                        String voie = currentAdresse.getThoroughfare();
                        if(currentAdresse.getSubThoroughfare() != null)
                            numero = Integer.parseInt(currentAdresse.getSubThoroughfare());

                        Adresse ad = new Adresse();

                        ad.setmCodePostal( Integer.parseInt(postalCode) );
                        ad.setmVille(currentAdresse.getLocality());
                        ad.setmCountry(currentAdresse.getCountryCode());
                        ad.setmVoie(voie);
                        ad.setmNum(numero);

                        list.add(ad);
                    }
                }
            }

        }


        return list;
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        parentContext = (android.support.v4.app.FragmentActivity) context;
        if (context instanceof OnFragmentMapInteractionListener) {
            mListener = (OnFragmentMapInteractionListener) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onNormalMap(View view) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    public void onSatelliteMap(View view) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
    }

    public void onTerrainMap(View view) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
    }

    public void onHybridMap(View view) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }

    public interface OnFragmentMapInteractionListener {
        // TODO: Update argument type and name
        void envoyerDepartement(Adresse adresse);
    }

}
